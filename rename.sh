#!/usr/bin/env bash

set -euo pipefail

# the keyword to be replaced in all files by the new project name
keyword="template"

# shellcheck disable=SC2034
backend_files=(
	"README.md"
	"docker/docker-compose.yml"
	"docker/manage.sh"
	"backend/Dockerfile"
	"backend/entrypoint.sh"
	"backend/manage.py"
	"backend/pyproject.toml"
	"backend/template/celery.py"
	"backend/template/settings.py"
	"backend/template/wsgi.py"
)

# exit if no name was given
if ! [ $# -eq 1 ]; then
	echo "Usage: rename.sh <new_project_name>"
	exit 1
fi

# ensures there are no `-` character that could break python modules
backend_name="${1/-/_}"

rename_files () {
	# first parameter is the list of files to be renamed
	project_name=$2
	# second parameter is the name of the project
	local -n files=$1
	for file in "${files[@]}"; do
		echo "replacing $keyword by $project_name in $file"
		# replaces all occurences of `template` except those starting with a .
		# since some django installed apps contains template
		if [ -f "$file" ];then
			sed -Ei '/\w+\.template/!s/'"$keyword"'/'"$project_name"'/g' "$file"
		fi
	done
}

rename_files backend_files "$backend_name"

# rename the backend/template folder
mv backend/"$keyword" backend/"$backend_name"

echo "backend => $backend_name"
