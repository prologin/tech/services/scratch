from django.contrib import admin

from .models import Event, ScratchProject

admin.site.register(Event)
admin.site.register(ScratchProject)
