from django.urls import include, path
from django_utils.urls import default

urlpatterns = default.urlpatterns() + [
    path("", include("scratch_show.urls")),
]
